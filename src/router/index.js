import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Cardapio from '../views/Cardapio.vue'
import Resumo from '../views/Resumo.vue'
import Ecobag from '../views/Ecobag.vue'
import Entrega from '../views/Entrega.vue'
import VueTheMask from 'vue-the-mask'

Vue.use(VueRouter)
Vue.use(VueTheMask)

  const routes = [
  {
    path: '/',
    name: 'cardapio',
    component: Cardapio
  },
  {
    path: '/home',
    name: 'home',
    component: Home
  },
  {
    path: '/entrega',
    name: 'entrega',
    component: Entrega
  },
  {
    path: '/resumo',
    name: 'resumo',
    component: Resumo
  },
  {
    path: '/ecobag',
    name: 'ecobag',
    component: Ecobag
  }
]

const router = new VueRouter({
  base: process.env.BASE_URL,
  routes,
  scrollBehavior (to, from, savedPosition) {
    return { x: 0, y: 0 }
  }
})


export default router
